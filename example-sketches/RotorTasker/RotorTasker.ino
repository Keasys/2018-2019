/**
 * Rotor versione a task, definire:
 * - plotter (frequente)
 * - status periodico "lento"
 * - lettura photocell (campionamento su array?)
 * - calcolo velocità (via photo, trovando il max ad esempio)
 * - lettura da seriale (ncurses?)
 * - regolazione velocità
 *
 * Interrupt calcolo periodo rotazione
 *
 * Togliere relay e passare a L298? (forse meglio relay che "si sente")
 *
 * BOARD = lolin esp32
 */

// ESP32 pins
#define ROTOR 12
#define PHOTO 15
#define REED 13

// costanti parametri config
#define SAMPLES 300
#define DUTY_PERIOD 1000
#define MAX_DUTY_PERC 80	// per non esagerare
#define MIN_DUTY_PERC 0	// per non fermarlo mai (che poi potrebbe non ripartire)
#define DEBOUNCE_MU 8000	// da tarare
#define MAXANALOG 4096

// variabili di stato (globali, ragionare su uso memoria)
volatile long lastTick=0;
volatile long lastPulse=0;
volatile long period=0;
portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;

int desiredRpm=100; // molto instabile
//int desiredRpm=200; // instabile
//int desiredRpm=300; // quasi stabile
//int desiredRpm=400; // più o meno stabile
//int desiredRpm=800; // chi lo sa...

int incremento=10; // per veder variare l'instabilità di inseguimento del desired

int photovalues[SAMPLES];
int cursor=0;

int absDuty=150; // default (check HIGH/LOW!)

/** restituisce il numero di giri al minuto, partendo dal periodo in millisec
 */
int rpm() {
    if(period!=0)
        return 60000000/period; // va bene anche divisione intera, attenzione al divByZero!!!
    return 0;
}

#include <TaskScheduler.h>
Scheduler runner;

/** plot su oled */
Task taskOled(10*TASK_MILLISECOND, TASK_FOREVER, statusOled);

/** stampa qualche semplice dato per rappresentazione "plotter" */
Task taskPlotter(100*TASK_MILLISECOND, TASK_FOREVER, []() {
    // ragionare sulle scale di rappresentazione

    Serial.print(rpm());

    Serial.print(",");
    Serial.print(desiredRpm);

    Serial.print(",");
    Serial.print(absDuty);

    /*
    Serial.print(",");
    Serial.print(period/1000);
    */

    Serial.println();
});

/** stampa uno stato più "parlante" per debugging */
Task taskStatus(2*TASK_SECOND, TASK_FOREVER, []() {
    Serial.println("---------------------------");

    Serial.print("rpm: ");
    Serial.println(rpm());

    Serial.print("desiredRpm: ");
    Serial.println(desiredRpm);

    Serial.print("absDuty: ");
    Serial.println(absDuty);

    Serial.print("lastPulse: ");
    Serial.println(lastPulse);

    Serial.print("lastTick: ");
    Serial.println(lastTick);

    Serial.print("period: ");
    Serial.println(period);

    // cursor e photovalues non ha senso per ora
});

/** campiona la fotoresistenza (per uso futuro) */
Task taskPhotocellRead(2*TASK_MILLISECOND, TASK_FOREVER, []() {
    photovalues[cursor] = analogRead(PHOTO);
    cursor++;
    if(cursor>=SAMPLES) cursor=0;
});

/** calcola duty in funzione della velocità desiderata, fare varie prove */
Task taskControl(50*TASK_MILLISECOND, TASK_FOREVER, []() {
    // esempio banale (inseguitore a incremento fisso)
    if(desiredRpm-rpm()>0)
        setDutyPercentage(getDutyPercentage()+incremento);
    else if(desiredRpm-rpm()<0)
        setDutyPercentage(getDutyPercentage()-incremento);
    else if(desiredRpm==0)
        setDutyPercentage(0);
});

/** legge input da seriale (interfaccia utente) e cambia i parametri di lavoro (velocità desiderata) */
Task taskSerial(2*TASK_SECOND, TASK_FOREVER, []() {
    if(Serial.available()>0) {
        //setDutyPercentage(Serial.readStringUntil('\n').toInt()); // NON pilotare direttamente duty, impostare un rpm desiderato e lasciare che sia il "controllo" a calcolare duty
        desiredRpm=Serial.readStringUntil('\n').toInt();
        Serial.println("...data received");
    }
});

/** controlla (accende/spegne) attivazione motore in base ai parametri */
Task taskMotor(10*TASK_MILLISECOND, TASK_FOREVER, []() {
    // ragionare sul <> in funzione del HIGH/LOW della board (a volte ribaltato)
    if(millis()%DUTY_PERIOD < absDuty) // così non è in percentuale, ma va rapportato a DUTY_PERIOD (cmq basta impostare absDuty via funzione "setter")
        //if(millis()%DUTY_PERIOD < 150) // versione fissa per testing
        digitalWrite(ROTOR, HIGH);
    else
        digitalWrite(ROTOR, LOW);
});

/** ISR  */
void reedRead() {
    noInterrupts(); // ESP8266, Arduino
    portENTER_CRITICAL(&mux); // ESP32

    long tick=micros();
    long tmpPeriod=tick-lastTick; // da validare, se dura più di DEBOUNCE

    // DONE nonostante tutto molti rimbalzi, introdurre "debounce"
    if(tmpPeriod>DEBOUNCE_MU) {
        period=2*tmpPeriod; // ci sono DUE magneti! altrimenti sarebbe un emiperiodo
        lastTick=tick;
        //Serial.print("*"); // ATTENZIONE! bruttissimo chiamare "cose complesse" da dentro una ISR!
    } //else
    //Serial.print("."); // ATTENZIONE! bruttissimo chiamare "cose complesse" da dentro una ISR!

    // provare pulsein? https://www.arduino.cc/reference/en/language/functions/advanced-io/pulsein/
    //lastPulse=pulseIn(REED,HIGH); // HIGH perché l'interrupt è sul FALLING
    // vedere quanto blocca questa invocazione... ;)

    portEXIT_CRITICAL(&mux); // ESP32
    interrupts(); // ESP8266, Arduino
}

/** getter per la percentuale */
int getDutyPercentage() {
    return map(absDuty,0,DUTY_PERIOD,0,100);
}

/** setter per la percentuale */
void setDutyPercentage(int perc) {
    /*
    Serial.print("# % richiesta:");
    Serial.println(perc);
    */

    if(perc>MAX_DUTY_PERC)
        perc=MAX_DUTY_PERC;

    if(perc<0)
        perc=0;

    if(desiredRpm>0 && perc==0)
        perc=MIN_DUTY_PERC;

    absDuty = map(perc,0,100,0,DUTY_PERIOD);
}

///////////////////////////////////////////////////////////////////////////////////
void setup() {
    Serial.begin(115200);
    Serial.println("< booting...");

    pinMode(ROTOR, OUTPUT);
    pinMode(REED, INPUT_PULLUP); // quindi il reed va messo fra pin e massa (sarà "attivo basso")

    // https://techtutorialsx.com/2017/09/30/esp32-arduino-external-interrupts/
    // e anche esempio GPIOinterrupt
    // timer interrupt: https://techtutorialsx.com/2017/10/07/esp32-arduino-timer-interrupts/

    attachInterrupt(digitalPinToInterrupt(REED), reedRead, FALLING);

    oled_setup();

    // tasks
    runner.init();

    runner.addTask(taskPlotter);
    runner.addTask(taskStatus);
    runner.addTask(taskPhotocellRead);
    runner.addTask(taskSerial);
    runner.addTask(taskMotor);
    runner.addTask(taskControl);
    runner.addTask(taskOled);

    taskPlotter.enable();
    //taskStatus.enable();
    taskPhotocellRead.enable();
    taskSerial.enable();
    taskMotor.enable();
    taskControl.enable();
    taskOled.enable();

    Serial.println("setup complete! >");
}

////////////////////////////////////////////////////////
void loop() {
    //Serial.println("loop...");
    //delay(500); // da non fare se si usa lo scheduler

    runner.execute(); // TaskScheduler
}



















/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 by Daniel Eichhorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

// Include the correct display library
// For a connection via I2C using Wire include
#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`
// or #include "SH1106.h" alis for `#include "SH1106Wire.h"`
// For a connection via I2C using brzo_i2c (must be installed) include
// #include <brzo_i2c.h> // Only needed for Arduino 1.6.5 and earlier
// #include "SSD1306Brzo.h"
// #include "SH1106Brzo.h"
// For a connection via SPI include
// #include <SPI.h> // Only needed for Arduino 1.6.5 and earlier
// #include "SSD1306Spi.h"
// #include "SH1106SPi.h"

// Initialize the OLED display using SPI
// D5 -> CLK
// D7 -> MOSI (DOUT)
// D0 -> RES
// D2 -> DC
// D8 -> CS
// SSD1306Spi        display(D0, D2, D8);
// or
// SH1106Spi         display(D0, D2);

// Initialize the OLED display using brzo_i2c
// D3 -> SDA
// D5 -> SCL
// SSD1306Brzo display(0x3c, D3, D5);
// or
// SH1106Brzo  display(0x3c, D3, D5);

// Initialize the OLED display using Wire library
SSD1306  display(0x3c, 5, 4);
// SH1106 display(0x3c, D3, D5);

void oled_setup() {
    // Initialising the UI will init the display too.
    display.init();

    display.flipScreenVertically();
    display.setFont(ArialMT_Plain_10);

    display.clear();
}

int x=0;
void statusOled() {
    int y=map(analogRead(PHOTO),0,MAXANALOG,64,0);
    display.setPixel(x,y);

    x++;
    if(x>128) {
        x=0;
        display.clear();
    }

    // write the buffer to the display
    display.display();
}
