# Sistemi Embedded 2018-2019

**Corso per laurea triennale**

A.A. 2018/2019 (SECONDO SEMESTRE)

  * [Programma ufficiale](http://www.ccdinfmi.unimi.it/it/corsiDiStudio/2018/F1Xof2/default/F1X-122/F1X-122.18.1/programma_it.html)
  * [Telegram](https://t.me/joinchat/ALnoPw8wxMVMet7DYU1NZQ)

Orari e aule:

* Lunedì, 11.30-13.30, aula Omega
* Venerdì, 11.30-13.30, aula Delta

(lezioni senza intervallo "inline", ma si comincia circa 10 min dopo orario ufficiale e si termina circa 10 min prima)

Modalità esame:

* orale/scritto (domande aperte)
* progetto da presentare in aula



## AVVISI
* Nei seguenti giorni NON CI SARA' LEZIONE:
  * 19 Aprile (vacanza)
  * 22 Aprile (vacanza)
  * 26 Aprile (vacanza)
  * 29 Aprile (mia assenza)
  * 17 Maggio (mia assenza)
* INIZIO CORSO: venerdì 1/3

* Gentile richiesta, che vale per tutte le pagine di questo repo: se notate cose da correggere fatelo pure, idem vale per l'elenco delle cose fatte (diario), aggiungete pure se mi sono dimenticato degli item, grazie!


## Libro di testo

Alexjan Carraturo, Andrea Trentini

SISTEMI EMBEDDED: TEORIA E PRATICA (seconda edizione) [[ledizioni](http://www.ledizioni.it/prodotto/a-carraturo-a-trentini-sistemi-embedded-teoria-pratica/)] [[amazon](https://www.amazon.it/Sistemi-embedded-pratica-Alexjan-Carraturo/dp/8867059432/)]


## Livello di conoscenza/approfondimento richiesto per ogni capitolo

Nota bene: per ora (marzo 2019) questo è il livello di approfondimento richiesto rispetto agli argomenti trattati nel testo, ma la situazione potrebbe variare (e ne discuteremo in aula) in funzione di cosa riusciremo a coprire a lezione e agli interventi esterni che riusciremo ad organizzare.

1. Intro: tutto, in dettaglio
1. Concetti: tutto, in dettaglio
1. Richiami: tutto, in dettaglio
1. Architetture: tutto, in dettaglio
1. Mem: tutto, in dettaglio
1. S.O.: overview
1. Linux: overview
1. FreeRTOS: overview
1. Arduino: tutto, in dettaglio
1. Rete: tutto, in dettaglio

* App A: overview
* App B: overview (anche perché questi argomenti saranno oggetto di lab)

P.S. man mano che scrivo (e pubblico) le domande d'esame mi rendo conto del livello di approfondimento che intendo per i vari capitoli



## Esami

Il calendario (sul SIFA) è sempre da considerarsi indicativo perché le sessioni d'esame si fanno su appuntamento da concordare col docente (via mail).

Quindi importante iscriversi al SIFA per avere poi la possibilità di verbalizzare, ma ancora più importante comunicare col docente le proprie intenzioni e lo stato dei lavori in modo da pianificare per tempo le sessioni.

Inoltre è FONDAMENTALE pubblicare con un certo anticipo il proprio repository di progetto sulla pagina "progetti" per dare modo al docente di valutare codice, architettura, etc.



## Comunicazioni col docente

Preferibilmente via mail (andrea punto trentini lumachella unimi punto it), importante mettere un subject "parlante" (es. "sistemi embedded, domanda su costante RC") in modo da identificare subito il contesto.

Non abbiate paura di scrivere, meglio una mail in più di una in meno, specie se per concordare l'esame (sia il progetto che la data effettiva di presentazione).



## Hardware consigliato

* ESP32
* ESP8266
* Arduino "classico"

## URL delle board aggiuntive

Da mettere nel "board manager" dell'IDE (File -> Preferences -> Additional Boards Manager)

```ssh
http://digistump.com/package_digistump_index.json
http://arduino.esp8266.com/stable/package_esp8266com_index.json
http://raw.githubusercontent.com/esp8266/Arduino/master/boards.txt
http://adafruit.github.io/arduino-board-index/package_adafruit_index.json
https://raw.githubusercontent.com/sparkfun/Arduino_Boards/master/IDE_Board_Manager/package_sparkfun_index.json
https://dl.espressif.com/dl/package_esp32_index.json
```

## Documentazione

FIXME inserire link doc ufficiale ESP32 => arduino

- Datasheet Atmel-42735-8-bit-AVR-Microcontroller-ATmega328/P [[download github](https://github.com/b1gtuna/rainbowduino/raw/master/Atmel-42735-8-bit-AVR-Microcontroller-ATmega328-328P_datasheet.pdf)]
- Utilizzo del VIN PIN come output/utilizzo di power supply esterni [[stackexchange]](https://arduino.stackexchange.com/a/51878)





## Scratchpad (temporaneo)

https://demo.codimd.org/oRIG4LK1SsaDsvm7FtCXpA
