/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file        
 *
 *  Configuration header for the BMA_280_cc.c file.
 *
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef BMA_280_CH_H_
#define BMA_280_CH_H_

/* local interface declaration ********************************************** */

/* local type and macro definitions */
#define PAD_THREESECONDDELAY            UINT32_C(3000) /**< one second is represented by this macro */
#define PAD_TIMERBLOCKTIME              UINT32_C(0xffff) /**< Macro used to define blocktime of a timer*/
#define PAD_USEI2CPROTOCOL              UINT8_C(0) /**< I2c protocol is represented by this macro*/
#define PAD_VALUE_ZERO 		            UINT8_C(0) /**< default value*/
#define PAD_I2C_ADDRESS                 UINT8_C(0x18) /**< i2c address for BMA280 */
#define PAD_FAILURE                     INT8_C(-1) /**< macro to failure state */
#define PAD_EVENNUMBER_IDENTIFIER       UINT8_C(2) /**< macro to identify even numbers */

/* local function prototype declarations */

/**
 * @brief Gets the raw data from BMA Accel and prints through the USB printf on serial port
 * @param[in] pvParameters Rtos task should be defined with the type void *(as argument)
 */
void bma280_getSensorValues(void *pvParameters);

/* local module global variable declarations */

/* local inline function definitions */

#endif /* BMA_280_CH_H_ */

/** ************************************************************************* */
